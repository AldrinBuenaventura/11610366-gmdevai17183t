﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {

    public float maxHP;
    public float HP;
    // public List<GameObject> targets;

    private void Start()
    {
        HP = maxHP;
    }

    void TakeDamage(float damage)
    {
        HP -= damage;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //targets.Add(collision.gameObject);

    }

    private void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.tag == "Shockwave")
        {
            TakeDamage(maxHP * 0.05f);
            if (HP <= 0)
                Destroy(this.gameObject);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        //if(collision.gameObject.tag == "Shockwave")
        //{ }
    }

}
