﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour {

    // Use this for initialization
    public GameObject Monsters;
    public Transform startPosition;

    public int monsterLimit = 5;
    public int monsterCounter = 0;
	void Start () {
            InvokeRepeating("Spawn", 0.0f, 0.5f);
	}

    void Spawn()
    {

            Instantiate(Monsters, new Vector3(startPosition.transform.position.x, 1, startPosition.transform.position.z), Quaternion.identity);
            monsterCounter++;
        
    }

    // Update is called once per frame
    void Update() {
        if (monsterCounter == monsterLimit)
            CancelInvoke();
    }
}
