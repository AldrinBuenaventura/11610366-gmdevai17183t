﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public int index;
    public List<Node> path;


    void Start()
    {
        path = GameObject.Find("GameManager").GetComponent<Pathfinding>().fullPath;
        index = 0;
    }

    void Update()
    {
        if (path.Count - 1 > index)
        {
            if (Vector3.Distance(new Vector3(this.transform.position.x, path[index].Position.y, this.transform.position.z), path[index].Position) > 0.1f)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(path[index].Position.x, 1, path[index].Position.z), 5.0f * Time.deltaTime);
            }
            else
            {
                index++;
            }
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
