﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    public int GridX; //X position in the node array
    public int GridY; //Y position in the node array

    public bool IsWall; //Tells if this node is being obstructed
    public Vector3 Position;

    public Node ParentNode; //For A* Algo, this will store what node it previously came from so it can trace the shortest path

    public int G_Cost; //The cost of moving to the next square
    public int H_Cost; //This distance to the goal from this node

    /**
     * A quick get function to add G and H
     * */
    public int F_Cost
    {
        get
        {
            return G_Cost + H_Cost;
        }
    }
    
    public Node(bool isWall, Vector3 pos, int gridX, int gridY)
    {
        this.IsWall = isWall;
        this.Position = pos;
        this.GridX = gridX;
        this.GridY = gridY;
    }
}
