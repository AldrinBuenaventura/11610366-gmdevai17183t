﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttack : MonoBehaviour {

    public GameObject Shots;
    public Transform Target;

    public List<GameObject> targets;

    private float distance;

    private void Start()
    {
        if (gameObject.tag == "STower")
            Invoke("Shoot", 1.5f);

        //if (gameObject.tag == "NTower")
        //    Invoke("ShootN", 1.0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Monster" && gameObject.tag == "NTower")
            targets.Add(collision.gameObject);
    }

    private void OnCollisionExit(Collision collision)
    {
        //if (collision.gameObject.tag == "Monster")
            targets.Remove(collision.gameObject);
    }

    void Update () {
        Debug.Log(targets.Count);

    }

    void Shoot()
    {
        Instantiate(Shots, new Vector3(this.transform.position.x, 0, this.transform.position.z), Quaternion.identity);
    }
}
    