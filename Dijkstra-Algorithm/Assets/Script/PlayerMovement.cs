﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script
{
    class PlayerMovement : MonoBehaviour
    {

        public List<Transform> paths;
        private Transform currentPath;
        private int index = 0;

        void Update()
        {
            if (index == 0)
                transform.position = paths[index].transform.position;
            if (paths.Count > 0)
            {
                if (Vector3.Distance(transform.position, paths[index].transform.position) > 0.1f)
                    transform.position = Vector3.MoveTowards(transform.position, paths[index].transform.position, 1);
                else
                    index++;
            }
        }

        public void StartMoving(List<Transform> paths)
        {
            this.paths.Clear();
            this.paths = paths;
        }
        
    }
}
